# VSCode settings files

Files should go into the folder:

	C:\Users\<user_name>\AppData\Roaming\Code\User


List extentions (works in PowerShell!):

	code --list-extensions | % { "code --install-extension $_" }
